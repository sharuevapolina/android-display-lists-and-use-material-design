package com.example.app30days.model

import com.example.a30days.R

object TipRepository {
    val Tips = listOf(
        Tip(
            nameRes = R.string.name_tip_1,
            descriptionRes = R.string.description_tip_1,
            imageRes = R.drawable.image_tip_1,
        ),
        Tip(
            nameRes = R.string.name_tip_2,
            descriptionRes = R.string.description_tip_2,
            imageRes = R.drawable.image_tip_2,
        ),
        Tip(
            nameRes = R.string.name_tip_3,
            descriptionRes = R.string.description_tip_3,
            imageRes = R.drawable.image_tip_3,
        ),
        Tip(
            nameRes = R.string.name_tip_4,
            descriptionRes = R.string.description_tip_4,
            imageRes = R.drawable.image_tip_4,
        ),
        Tip(
            nameRes = R.string.name_tip_5,
            descriptionRes = R.string.description_tip_5,
            imageRes = R.drawable.image_tip_5,
        ),
        Tip(
            nameRes = R.string.name_tip_6,
            descriptionRes = R.string.description_tip_6,
            imageRes = R.drawable.image_tip_6,
        ),
        Tip(
            nameRes = R.string.name_tip_7,
            descriptionRes = R.string.description_tip_7,
            imageRes = R.drawable.image_tip_7,
        ),
        Tip(
            nameRes = R.string.name_tip_8,
            descriptionRes = R.string.description_tip_8,
            imageRes = R.drawable.image_tip_8,
        ),
        Tip(
            nameRes = R.string.name_tip_9,
            descriptionRes = R.string.description_tip_9,
            imageRes = R.drawable.image_tip_9,
        ),
        Tip(
            nameRes = R.string.name_tip_10,
            descriptionRes = R.string.description_tip_10,
            imageRes = R.drawable.image_tip_10,
        ),
        Tip(
            nameRes = R.string.name_tip_11,
            descriptionRes = R.string.description_tip_11,
            imageRes = R.drawable.image_tip_11,
        ),
        Tip(
            nameRes = R.string.name_tip_12,
            descriptionRes = R.string.description_tip_12,
            imageRes = R.drawable.image_tip_12,
        ),
        Tip(
            nameRes = R.string.name_tip_13,
            descriptionRes = R.string.description_tip_13,
            imageRes = R.drawable.image_tip_13,
        ),
        Tip(
            nameRes = R.string.name_tip_14,
            descriptionRes = R.string.description_tip_14,
            imageRes = R.drawable.image_tip_14,
        ),
        Tip(
            nameRes = R.string.name_tip_15,
            descriptionRes = R.string.description_tip_15,
            imageRes = R.drawable.image_tip_15,
        ),
        Tip(
            nameRes = R.string.name_tip_16,
            descriptionRes = R.string.description_tip_16,
            imageRes = R.drawable.image_tip_16,
        ),
        Tip(
            nameRes = R.string.name_tip_17,
            descriptionRes = R.string.description_tip_17,
            imageRes = R.drawable.image_tip_17,
        ),
        Tip(
            nameRes = R.string.name_tip_18,
            descriptionRes = R.string.description_tip_18,
            imageRes = R.drawable.image_tip_18,
        ),
        Tip(
            nameRes = R.string.name_tip_19,
            descriptionRes = R.string.description_tip_19,
            imageRes = R.drawable.image_tip_19,
        ),
        Tip(
            nameRes = R.string.name_tip_20,
            descriptionRes = R.string.description_tip_20,
            imageRes = R.drawable.image_tip_20,
        ),
        Tip(
            nameRes = R.string.name_tip_21,
            descriptionRes = R.string.description_tip_21,
            imageRes = R.drawable.image_tip_21,
        ),
        Tip(
            nameRes = R.string.name_tip_22,
            descriptionRes = R.string.description_tip_22,
            imageRes = R.drawable.image_tip_22,
        ),
        Tip(
            nameRes = R.string.name_tip_23,
            descriptionRes = R.string.description_tip_23,
            imageRes = R.drawable.image_tip_24,
        ),
        Tip(
            nameRes = R.string.name_tip_25,
            descriptionRes = R.string.description_tip_25,
            imageRes = R.drawable.image_tip_25,
        ),
        Tip(
            nameRes = R.string.name_tip_26,
            descriptionRes = R.string.description_tip_26,
            imageRes = R.drawable.image_tip_26,
        ),
        Tip(
            nameRes = R.string.name_tip_27,
            descriptionRes = R.string.description_tip_27,
            imageRes = R.drawable.image_tip_27,
        ),
        Tip(
            nameRes = R.string.name_tip_28,
            descriptionRes = R.string.description_tip_28,
            imageRes = R.drawable.image_tip_28,
        ),
        Tip(
            nameRes = R.string.name_tip_29,
            descriptionRes = R.string.description_tip_29,
            imageRes = R.drawable.image_tip_29,
        ),
        Tip(
            nameRes = R.string.name_tip_30,
            descriptionRes = R.string.description_tip_30,
            imageRes = R.drawable.image_tip_30,
        ),

        )
}